import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import dash_bootstrap_components as dbc
import pandas as pd
import requests
import pandas as pd
import numpy as np
import plotly.express as px
import geopandas as gpd
import json

thailand_geojson = gpd.read_file("/home/athip/psu/3/ecosys/dash_prj6510110513/dash_proj/dash_proj/resource/provinces.geojson")
thailand_geojson = thailand_geojson.sort_values(by=["pro_th"])

json_file_path = '/home/athip/psu/3/ecosys/dash_prj6510110513/dash_proj/dash_proj/resource/center_province_of_thailand_th.json'

with open(json_file_path, 'r') as center_of_province:
    province_centers = json.load(center_of_province)


def preprocess():
    url = "https://gpa.obec.go.th/reportdata/pp3-4_2566_province.json?fbclid=IwZXh0bgNhZW0CMTAAAR3sp-51uAuC9Z86aBVBudyAdLPE7sF3WXXGeoC50AAteVAdt1nq097-ECg_aem_D1Dal_3AF65dhoA7as660Q"
    response = requests.get(url)
    data = response.json()
    df = pd.json_normalize(data)
    return df.sort_values(by=["schools_province"])

df = preprocess()

app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP], assets_folder='resource', assets_url_path='/resource/')

# Define the layout of the app
app.layout = dbc.Container([
    dcc.Interval(
    id="load_interval", 
    n_intervals=0, 
    max_intervals=0,
    interval=1,
    ),
    # html.Img(src='resource/istg.png'),
    dbc.Row([
        dbc.Col(html.H1("Summary Data Dashboard"), className="mb-2",style={"text-align":"center","margin-top":"20px","margin-bottom":"20px"})
    ]),
    dbc.Row([

        html.Div([
            dbc.Card([dbc.CardHeader("Number of male",style={"color":"black","text-align":"center"}),
                    dbc.CardBody([html.P(df["totalmale"].astype(int).sum(),style={"color":"black","text-align":"center"})]),]
                    , color="white", inverse=True,style={"width":"50vw","max-width":"350px","margin":"auto"}),
            dbc.Card([dbc.CardHeader("Number of female",style={"color":"black","text-align":"center"}),
                    dbc.CardBody([html.P(df["totalfemale"].astype(int).sum(),style={"color":"black","text-align":"center"})])]
                    , color="white", inverse=True,style={"width":"50vw","max-width":"350px","margin":"auto"}),
            dbc.Card([dbc.CardHeader("Number of total",style={"color":"black","text-align":"center"}),
                    dbc.CardBody([html.P(df["totalstd"].astype(int).sum(),style={"color":"black","text-align":"center"})])]
                    , color="white", inverse=True,style={"width":"50vw","max-width":"350px","margin":"auto"}),
                ],style={"display":"flex","justify-content": "center","margin":"auto","width":"1200px"}) #"display": "flex","justify-content": "center"

                ],style={"margin-bottom":"20px"}),

    dbc.Row([
        dbc.Col([
            dbc.Card([
                dbc.CardHeader(
                    dcc.Dropdown(
                        id='school-dropdown',
                        options=[{'label': area, 'value': area} for area in df['schools_province'].unique()],
                        # value=df['schools_name'].unique()[0],
                        multi=False,
                        clearable=False,
                        style={"width":"50vw","max-width":"350px"}
                                ),style={"color":"black","height":"60px"}),
                dbc.CardBody([
                    dcc.Graph(id='bar-chart')])],
                color="white", inverse=True),
        ]),
        dbc.Col([
            dbc.Card([
                dbc.CardHeader(
                    html.P("แผนที่แสดงจำนวนนักเรียนในชั้นอุดมศึกษาที่จบการศึกษาในปี 2566",style={"text-align":"center"}),style={"color":"black","height":"60px"}),
                dbc.CardBody([
                    dcc.Graph(id="graph_map")])
                    ],
                color="white", inverse=True),
                ])
        
    ]),
html.Div(id='click-data', style={'whiteSpace': 'pre-line'}),], fluid=True,style={'background-image': 'url("")', 
                             'background-size': 'cover', 'background-repeat': 'no-repeat',
                             'background-position': 'center', 'height': '100vh'})

times = 0
@app.callback(
    Output('bar-chart', 'figure'),
    [Input('school-dropdown', 'value'),Input("load_interval","n_intervals"),Input('graph_map', 'clickData')]
)
def update_chart(*selected_area):
    global times
    if times == 0:
        selected_area = df['schools_province'][0]
        times = 1
    elif selected_area[2] == None : 
        selected_area = selected_area[0]
    else : 
        selected_area = selected_area[2]['points'][0]['location']
    filtered_df = df[df['schools_province'] == selected_area]
    figure = {
        'data': [
            {'x': filtered_df.columns[ic], 'y': pd.Series(filtered_df.iloc[:,ic].astype(int).sum()), 'type': 'bar', 'name': filtered_df.columns[ic]} for ic in [3,4]
        ],
        'layout': {
            'title': f'{selected_area}'
        }
    }
    return figure

@app.callback(
    Output("graph_map",'figure'),
    [Input("load_interval","n_intervals"),Input('school-dropdown', 'value')]
)
def update_chart(n_intervals,school):
    # provinces = thailand_geojson['NAME_1'][:77]
    if school is not None :
        center = province_centers[school]
        zoom = 8
    else:
        center = {"lat": 13.736717, "lon": 100.523186}
        zoom = 3.5
    provinces = df["schools_province"]
    data = pd.DataFrame({
        'province': provinces,
        'value': df["totalstd"].astype(int).to_numpy()
    })

    fig = px.choropleth_mapbox(
        data_frame=data,
        geojson=thailand_geojson,
        locations='province',
        featureidkey='properties.pro_th',
        color='value',
        hover_name='province',
        color_continuous_scale='Viridis',
        mapbox_style="carto-positron",
        center=center,
        zoom=zoom
    )

    # Update the layout for better visualization
    fig.update_geos(fitbounds="locations", visible=False)
    fig.update_layout(title_text='Thailand Choropleth Map', title_x=0.5)
    fig.update_polars

    return fig

if __name__ == '__main__':
    app.run_server(debug=True)
